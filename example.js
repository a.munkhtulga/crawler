const express = require('express');
const app = express()
const port = 3000
const puppeteer = require('puppeteer');
const username = "hr@gerege.mn"
const password = "88082570 "
var finaldata = []
var browser
setTimeout(() => {
    initpage()
}, 100);
async function initpage() {
    browser = await puppeteer.launch({ args: ['--no-sandbox'], headless: false });
    let browserPage = await browser.pages()
    await browserPage[0].goto('http://localhost:3000/', {
        waitUntil: 'networkidle0',
            timeout: 0
        });
}

app.get('/', async (req, res) => {
    test();
    res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

function test(){
   (async () => {
        const tab = await browser.newPage();

        await tab.goto('https://www.zangia.mn', {
        waitUntil: 'networkidle0',
            timeout: 0
        });
    
       await tab.waitForSelector('a[href="account/login"]');
       await tab.click('a[href="account/login"]');

       await tab.waitForSelector('input[name=user]', { visible: true });
       await tab.focus('input[name=user')
       await tab.keyboard.type(username)

       await tab.focus('input[name=pass')
       await tab.keyboard.type(password)

       await tab.waitForSelector('a[href="javascript:;"]', { visible: true });
       await testDelay(3000);
       await tab.click('a[href="javascript:;"]');

       await tab.waitForSelector('a[href="anket/base/tab.offered"]', { visible: true });
       await tab.click('a[href="anket/base/tab.offered"]');
       await testDelay(3000);

       await tab.waitForSelector('a[href="anket/base/tab.search"]', { visible: true });
       await testDelay(3000);
       await tab.click('a[href="anket/base/tab.search"]');
       await testDelay(1000);
       
       await tab.goto('https://www.zangia.mn/anket/base/tab.search/n.4/b.4/e.447/f.64/s.12/lmt.50/pg.1', {
        waitUntil: 'networkidle0',
            timeout: 0
        });

        await tab.waitForSelector('.cv_list', { visible: true, timeout: 50000 })
       
       const data = await tab.evaluate(() => {
        let elements = Array.from(document.querySelectorAll('.cv_list .cv .wb .off-btm a'));
        let links = elements.map(element => {
            return element.href.substring(22)
        })
            return links;
        });

         let pageData = []
       for (let index = 0; index < data.length; index++) {
           
           const tab1 = await browser.newPage();

            await tab1.goto('https://www.zangia.mn/' +  data[index], {
            waitUntil: 'networkidle0',
                timeout: 0
            });
           await testDelay(3000);

           const data1 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #main-info .minfo .data'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data2 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #experience .expr .data em'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data3 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #experience .expr .date span'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data4 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #experience .expr .data span'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data5 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #education .edu .data span'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });
           
           const data6 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #education .edu .data em'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data7 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #education .edu .data b b'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data8 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #education .edu .date span'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data9 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #main-jobmore div b'));
               let links1 = elements1.map(element1 => {
                   
                return element1.innerText
            })
                return links1;
           });

           const data10 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #skills #skills1 .grp .skills .skill div .date'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data11 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #main-work ol li em'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data12 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #main-work ol li span'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data13 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.anket .head-section #main-contact .email span'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data14 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.anket .head-section #main-contact .mobile span'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data15 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.anket .head-section #main-contact .address span'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data16 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.anket .cols-holder .listed .about #main-about p'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data17 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #training .training div .date'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data18 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #training .training div .data'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data19 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #training .training div .data span'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data20 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #skills #skills2 .grp .skills .skill div .date'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });

           const data21 = await tab1.evaluate(() => {
            let elements1 = Array.from(document.querySelectorAll('.listed .sub-section #skills #skills2 .grp .skills .skill div .data'));
               let links1 = elements1.map(element1 => {

                return element1.innerText
            })
                return links1;
           });
        
           const objNames = ["family_name", "date_of_birth", "last_name", "reg_no", "first_name", "married", "gender", "driver_license", "phone", "email", "location", "my_about", "work_experience", "to_work", "education", "other_info", "other_training", "language", "skill"];
           let perdata = {}
           let perdata1 = []
           let perdata2 = []
           let perdata3 = []
           let perdata4 = []
           let perdata5 = []
           let perdata6 = []
           let perdata7 = []

           let ar = []
            for(var i = 1; i < data4.length; i += 2) {  // take every second element
                ar.push(data4[i]);
           }
           
           for (let i = 0; i < data2.length; i++) { 
               let asd = {}
               asd['company'] = ar[i];
                asd['position'] = data2[i];
                asd['date'] = data3[i];
                perdata1.push(asd);
           }
           
           for (let i = 0; i < data5.length; i++) { 
                let myObj = {}
                myObj['school'] = data5[i];
                myObj['edu'] = data6[i];
               myObj['result'] = data7[i];
               myObj['date'] = data8[i];
                perdata2.push(myObj);
           }
           
            let myObj = {}
            myObj['salary'] = data9[0];
            myObj['time'] = data9[1];
           perdata3.push(myObj);

           for (let i = 0; i < data10.length; i++) { 
                let myObj = {}
                myObj['description'] = data10[i];
                perdata4.push(myObj);
           }

           for (let i = 0; i < data17.length; i++) { 
                let myObj = {}
            //    myObj['location'] = data19[i];
               myObj['work'] = data18[i].replace('\n', ', (') + ')';
               myObj['date'] = data17[i];
                perdata6.push(myObj);
           }

           for (let i = 0; i < data11.length; i++) { 
                let myObj = {}
                myObj['description'] = data11[i];
                myObj['position'] = data12[i];
                // asd['comp'] = data4[i];
                perdata5.push(myObj);
           }

           for (let i = 0; i < data20.length; i++) { 
                let myObj = {}
                myObj['name'] = data20[i];
                myObj['percent'] = data21[i];
                // asd['comp'] = data4[i];
                perdata7.push(myObj);
           }
     
           for (let index = 0; index < objNames.length; index++) {
               if (objNames[index] == "work_experience") {                  
                   perdata[objNames[index]] = perdata1
               }
               else if (objNames[index] == "phone") {
                   perdata[objNames[index]] = data14[0]
               } 
               else if (objNames[index] == "email") {
                   perdata[objNames[index]] = data13[0]
               } 
                else if (objNames[index] == "location") {
                   perdata[objNames[index]] = data15[0]
               }
                else if (objNames[index] == "my_about") {
                   perdata[objNames[index]] = data16[0]
               }
               else if (objNames[index] == "to_work") {
                   perdata[objNames[index]] = perdata5
               }
                else if (objNames[index] == "education") {
                   perdata[objNames[index]] = perdata2
               }
               else if (objNames[index] == "other_info") {
                   perdata[objNames[index]] = perdata3
               }
                   else if (objNames[index] == "other_training") {
                   perdata[objNames[index]] = perdata6
               }
                else if (objNames[index] == "language") {
                   perdata[objNames[index]] = perdata7
               }
               else if (objNames[index] == "skill") {
                   perdata[objNames[index]] = perdata4
               }
               else {
                   perdata[objNames[index]] = data1[index]
               }
           }
           pageData = await pageData.concat(perdata)
           tab1.close();
           await testDelay(2000);

       }


       console.log("урт = ", pageData.length)
       console.log("дата", pageData)
       const fs = require('fs');
        fs.writeFileSync("data", JSON.stringify(pageData));
       
       console.log("end")
       browser.close()
       return pageData



    //    if (data.length > 1) {
    //        let pageData = []
    //        let b = await getPageData(tab)
    //         pageData = pageData.concat(b)
    //         for (let index = 1; index < data.length; index++) {
    //             let string = "a[href='" + data[index] + "']"
    //             console.log("Дараагын хуудасруу шилжиж байна")
    //             await tab.click(string);
    //             await tab.waitForSelector('strong', { visible: true, timeout: 90000 });
    //             let c = await getPageData(tab)
    //             pageData = await pageData.concat(c)
    //        }
    //        finaldata = await finaldata.concat(pageData)
    //    } else {
    //        let f = await getPageData(tab)
    //         finaldata = await finaldata.concat(f)
    //    }
    //    console.log("Дата-ны нийт урт = ", finaldata.length)
    //    console.log("Нийт дата", finaldata)

    //    sendData(0)
       
    })();

    const testDelay = (delayInms) =>  {
        return new Promise(resolve => {
        setTimeout(() => {
            resolve(2);
        }, delayInms);
        });
    }

    // async function sendData(inde) {
    //     console.log("index", inde)
    //     if (inde != finaldata.length) {
    //     let target
    //     if (inde + 1500 < finaldata.length) {
    //         target = inde + 1500
    //     } else {
    //         target = finaldata.length
    //         }
    //         console.log("target", target)
    //         let perdata = await finaldata.slice(inde, target)
    //         console.log("perdata Урт = ",perdata.length)
    //         // await axios.post()
    //         sendData(target)
    //     } else {
    //         // browser.close()
    //         console.log("Амжилттай Дууслаа")
    //     }
    // }
    
}
